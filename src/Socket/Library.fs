namespace Socket

open System.Net

module IPC =

    let openSocket(path : string) =
        let sock = new Sockets.Socket(Sockets.AddressFamily.Unix, Sockets.SocketType.Stream, Sockets.ProtocolType.Unspecified)
        let domainSock = Sockets.UnixDomainSocketEndPoint(path)

        sock.Connect(domainSock)

        let msg = "HELLO HELLO HI"B

        sock.Send(msg)
