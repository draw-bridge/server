// Learn more about F# at http://fsharp.org
open System
open Chiron
open Suave
open Suave.Writers
open Suave.Filters
open Suave.Operators
open Suave.Successful

let corsConfig =
    { CORS.defaultCORSConfig with allowedUris =
                                      CORS.InclusiveOption.Some
                                          [ "http://127.0.0.1:8080" ] }

type SVGData =
    { Name : string }
    static member ToJson(svg : SVGData) = Json.write "name" svg.Name

let testSerialize =
    { Name = "stunt_svg" }
    |> Json.serialize
    |> Json.format

let webApp =
    choose
        [ GET
          >=> choose
                  [ path "/api" >=> OK "Found the API"

                    path "/api/data" >=> CORS.cors corsConfig
                    >=> OK testSerialize >=> setMimeType "application/json"
                    >=> addHeader "X-Content-Type-Option" "nosniff" ] ]

// [<EntryPoint>]
// let main argv =
//     printfn "Hello World from F#!"
//     startWebServer
//         { defaultConfig with bindings =
//                                  [ HttpBinding.create HTTP
//                                        Net.IPAddress.Loopback 5000us ] } webApp
//     0

[<EntryPoint>]
let main argv =
    Socket.IPC.openSocket("/tmp/blap.sock")
