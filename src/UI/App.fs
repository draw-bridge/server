module App.View

open Elmish
open Elmish.Browser.Navigation
open Elmish.Browser.UrlParser

open Fable.Core
open Fable.Core.JsInterop
open Fable.Helpers
open Fable.Import
open Fable.Import.Browser
open Fable.PowerPack

open Types
open App.State
open Global

// NOTE: it's not _at all_ clear why this is needed. Commenting it out for a bit; if CSS breaks, this is probably why.
// importAll "../../sass/main.scss"

// NOTE: these three _were_ way down at the bottom. I dislike that, so I've
// scooted them. So far, no ill side-effect; not honestly sure what ill
// side-effects would look like here :/
open Elmish.React
open Elmish.Debug
open Elmish.HMR

let getAnSvg() =
    Fetch.fetch "http://127.0.0.1:5000/api/data" [Fetch.Fetch_types.RequestProperties.Mode Fetch.Fetch_types.RequestMode.Cors]
    |> Promise.bind (fun r -> r.text())
    |> Promise.map console.log

let menuItem label page currentPage =
    React.li
        [ ]
        [ React.a
            [ React.classList [ "is-active", page = currentPage ]
              React.Props.Href (toHash page) ]
            [ React.str label ] ]

let menu currentPage =
    React.aside
        [ React.Props.ClassName "menu" ]
        [ React.p
            [ React.Props.ClassName "menu-label" ]
            [ React.str "General" ]
          React.ul
            [ React.Props.ClassName "menu-list" ]
            [ menuItem "Home" Home currentPage
              menuItem "Counter sample" Counter currentPage
              menuItem "About" Page.About currentPage ] ]

let root model dispatch =

    let pageHtml =
        function
        | Page.About -> Info.View.root
        | Counter -> Counter.View.root model.counter (CounterMsg >> dispatch)
        | Home -> Home.View.root model.home (HomeMsg >> dispatch)

    React.div
        []
        [ React.div
            [ React.Props.ClassName "navbar-bg" ]
            [ React.div
                [ React.Props.ClassName "container" ]
                [ Navbar.View.root ] ]
          React.div
            [ React.Props.ClassName "section" ]
            [ React.div
                [ React.Props.ClassName "container" ]
                [ React.div
                    [ React.Props.ClassName "columns" ]
                    [ React.div
                        [ React.Props.ClassName "column is-3" ]
                        [ menu model.currentPage ]
                      React.div
                        [ React.Props.ClassName "column" ]
                        [ pageHtml model.currentPage ] ] ] ] ]



// App
Program.mkProgram init update root
|> Program.toNavigable (parseHash pageParser) urlUpdate
#if DEBUG
|> Program.withDebugger
#endif
|> Program.withReact "elmish-app"
|> Program.run
