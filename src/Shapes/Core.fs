namespace DrawBridge

(* Okay so. A line is a an equation, mapping x and y, and an extent -- a start
    and an end. A circle, and many closed shapes, will be the same. Rectangles
    -- indeed, many polygons -- will be a set of lines and extents. *)

module Core =

    type X = float
    type Y = float
    type Step = float

    /// <summary>Oh my god is this documentation?</summary>
    type Point =
        { X: float
          Y: float
          }

    // type Equation = Y -> X

    type IElement =
        abstract member Render: Step -> Point list

    /// The general equation for points on a circle is:

    /// (x−h)^2 + (y−k)^2 = r^2

    /// Thus, to achieve a more useful y = (things)x form:
    /// y = (√r^2 - (x-h)^2) + k
    type Circle(center: Point, radius: float) =
        member this.Center = center
        member this.Radius = radius

        member this.Equation x =
            let subtractant = pown (x - this.Center.X) 2
            let radiusSquared = pown this.Radius 2
            let root = sqrt (radiusSquared - subtractant |> float)
            root + this.Center.Y

        interface IElement with
            member this.Render step =
                let p = {X=1.0; Y=1.0}
                [p]

module SVG =

    type LineCommand =
        Move of point : Core.Point
        | Line of start : Core.Point * stop : Core.Point
        | Horizontal of stop : Core.Point
        | Vertical of stop : Core.Point
        | Close

    type CurveCommand =
        Cubic of stop : Core.Point * controlOne : Core.Point * controlTwo : Core.Point
        | ChainedCubic
        | Quadratic of stop : Core.Point * control : Core.Point
        | ChainedQuadratic

    // let private renderCircle circle : Core.Circle =

    // let render shape : Shape =
